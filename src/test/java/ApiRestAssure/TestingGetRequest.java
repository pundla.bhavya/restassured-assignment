package ApiRestAssure;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class TestingGetRequest {
  @Test
  public void FirstGetMethod() {
	  
	Response response=  RestAssured.get("https://gorest.co.in/public/v2/users");
	  int statusCode=response.getStatusCode();
	  System.out.println(statusCode);
	  System.out.println(response.getBody().asString());
  }
  @Test
  public void AssertGetMethod() {
	  Response response=  RestAssured.get("https://gorest.co.in/public/v2/users");
	  int statusCode=response.getStatusCode();
	  Assert.assertEquals(200, statusCode);
  }
}
