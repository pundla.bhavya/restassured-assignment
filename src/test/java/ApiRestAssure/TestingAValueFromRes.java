package ApiRestAssure;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.*;

public class TestingAValueFromRes {
  @Test
  public void TestingAValue() {
	  baseURI="https://reqres.in";
	  given().get("/api/users?page=2").then().body("data.first_name", hasItem("Byron"));
  }
  public void TestingAMultipleValues() {
	  baseURI="https://reqres.in";
	  given().get("/api/users?page=2").then().body("data.first_name",hasItem("Byron")).body("data.last_name",hasItem("Lawson"));
  }
}
