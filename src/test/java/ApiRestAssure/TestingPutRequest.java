package ApiRestAssure;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.baseURI;

import static org.hamcrest.Matchers.*;

public class TestingPutRequest {
  @Test
  public void UpdatingDataUsingPut() {
	  JSONObject request=new JSONObject();
	  request.put("name", "bhavya");
	  request.put("job", "tester");
	 // System.out.println(request);
	  
	  given().body(request.toJSONString())
	  .put("https://reqres.in/api/users/2")
	  .then()
	  .statusCode(200).body("updatedAt", greaterThanOrEqualTo("2023-01-24T09:50:12.293z"));
  }
	  
}
