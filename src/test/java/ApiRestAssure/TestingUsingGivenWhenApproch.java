package ApiRestAssure;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
public class TestingUsingGivenWhenApproch {
  @Test
  public void testStatusCode() {
	  
	  given().get("/api/iusers?page=2").then().statusCode(200);
	  
  }
  @Test
  public void testparticularValue() {
	  given().get("/api/iusers?page=2").then().body("data[1].email", equalTo("tobias.funke@reqres.in"));
  }
  @Test
  public void printAllValues() {
	  given().get("/api/iusers?page=2").then().log().all();
}
}