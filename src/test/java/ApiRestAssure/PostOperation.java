package ApiRestAssure;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;


public class PostOperation {
  @Test
  public void PostOperationTest() {
	  JSONObject request=new JSONObject();
	  request.put("name", "bhavya");
	  request.put("job", "tester");
	  System.out.println(request);
	  
	  baseURI="https://reqres.in/api";
	  given().body(request.toJSONString()).when().post("/users").then().statusCode(201);
	  
  }
}
